import { useState } from 'react';
import AppApi from '../utils/axios';

const UseTasker = () => {

    const [tasker, setTasker] = useState(null);
    const [taskerById, setTaskerById] = useState(null);
    const [isTaskerError, setError] = useState(false);
    const [isTaskerLoading, setLoading] = useState(false);
    
    const postTasker = async (user_id, values) => {
        const res = await AppApi('post', '/create-tasker', { user_id, ...values });
        return res;
    }

    const getTasker = async (user_id) => {
        const data = await AppApi('get', `/user-tasker?id=${user_id}`);

        if (!data) {
            return null;
        } else {
            return data;
        }
    }

    const getTaskerByTaskerId = async (tasker_id) => {
        const data = await AppApi('get', `/tasker?id=${tasker_id}`);
        if (!data) {
            return null;
        } else {
            return data;
        }
    }

    const updateTasker = async (values) => {
        const res = await AppApi('put', `/update-tasker`, values);
        return res;
    }

    const requestTasker = async (user_id) => {
        setLoading(true);
        setError(false);

        const data = await getTasker(user_id);
        
        setLoading(false);
        setTasker(data);
    }

    const requestTaskerById = async (tasker_id) => {
        setLoading(true);
        setError(false);

        const data = await getTaskerByTaskerId(tasker_id);
        
        setLoading(false);
        setTaskerById(data);
    }

    return {
        postTasker,
        getTasker,
        updateTasker,
        requestTasker,
        requestTaskerById,
        taskerById,
        tasker,
        isTaskerError,
        isTaskerLoading
    }
}

export default UseTasker;