import React, { useState, useEffect, useCallback } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import Slider from '@mui/material/Slider';
import CustomButton from '../../components/CustomButton';
import useTaskForm from '../../hooks/useTaskForm';
import SearchBar from '../../components/SearchBar';
import LoadingPage from '../../components/LoadingPage';
import { sortTypeSwitch } from './utils/SelectionItems';
import Map from '../../components/Map';
import { Display, Browse, ToolBar, LocationBox, MapBox } from './BrowsePage.style';
import TaskDetailCard from '../../components/TaskDetailCard';
import useComment from '../../hooks/useComment';
import useOffer from '../../hooks/useOffer';

const BrowsePage = () => {
  const navigate = useNavigate();
  const { task_id } = useParams();
  const currentUser = useSelector((state) => state.currentUser);
  const { submitRequest, getTaskById, isLoading, tasks, taskById } = useTaskForm();
  const { commentsRequest, comments } = useComment();
  const { offersRequest, offers } = useOffer();
  const [type, setType] = useState('a');
  const [budget, setBudget] = useState([10, 1000]);
  const [sortType, setSortType] = useState('a');
  const [suburb, setSuburb] = useState(
    currentUser.user.street ? `${currentUser.user.street}, ${currentUser.user.state}` : 'Sydney, NSW'
  );
  const [distance, setDistance] = useState(25);
  const [isSearch, setSearch] = useState(false);
  const [isTypeError, setTypeError] = useState(false);
  const [originTask, setOriginTask] = useState(null);
  const [displayTask, setTask] = useState(null);
  const [taskId, setTaskId] = useState(null);
  const [isResetTask, setRestTask] = useState(false);
  const [isFilter, setIsFilter] = useState(false);
  const [isTaskLoading, setIsTaskLoading] = useState(false);

  const backToBrowser = useCallback(() => navigate('/browse-tasks', { replace: true }), [navigate]);

  useEffect(() => {
    if (isResetTask) {
      commentsRequest(displayTask.id);
      offersRequest(displayTask.id);

      setRestTask(false);
    }
    if (taskId !== task_id && task_id) {
      setTaskId(task_id);
      getTaskById(task_id);
      commentsRequest(task_id);
      offersRequest(task_id);
    }
    if (!task_id && taskId !== task_id) {
      setTaskId(null);
      getTaskById(null);
    }
    if (originTask !== taskById) {
      setOriginTask(taskById);
      setTask(() => {
        if (taskById) {
          if (currentUser.followedTasks?.map((follow) => follow.id).includes(taskById.id)) {
            return { ...taskById, following: true };
          } else {
            return { ...taskById, following: false };
          }
        } else {
          return taskById;
        }
      });
    }
  }, [
    commentsRequest,
    offersRequest,
    displayTask,
    isResetTask,
    task_id,
    taskId,
    getTaskById,
    taskById,
    currentUser,
    originTask,
  ]);

  const chooseTask = useCallback((task) => {
    setTask(task);
    setRestTask(true);
  }, []);

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };

  const handleSortTypeChange = (event) => {
    setSortType(event.target.value);
  };

  const submitSearch = (keyword) => {
    setSearch(true);
    submitRequest(`${suburb}, au`, parseInt(distance * 1000), keyword);
    setIsTaskLoading(true);
    setTimeout(() => {
      setIsTaskLoading(false);
    }, 4000);
    backToBrowser();
  };

  const handleReset = () => {
    setBudget([10, 1000]);
    setSuburb(currentUser.user.street ? `${currentUser.user.street}, ${currentUser.user.state}` : 'Sydney, NSW');
    setDistance(25);
    setTypeError(false);
    setType('a');
    setSortType('a');
    submitRequest(' ', 0, ' ');
    setSearch(false);
    backToBrowser();
  };

  const tasksToDisplay = useCallback(
    (displayTasks) => {
      if (tasks) {
        let filteredTasks = Object.values(displayTasks)
          ?.filter((task) => task.category.includes(type))
          .filter(sortTypeSwitch(sortType))
          .filter((task) => task.budget > budget[0] && task.budget < budget[1]);
        if (!_.isEmpty(currentUser.followedTasks)) {
          const followingTasks = currentUser.followedTasks.map((follow) => follow.id);
          filteredTasks = filteredTasks.map((task) => {
            return { ...task, following: followingTasks.includes(task.id) };
          });
        }
        return filteredTasks;
      } else {
        return [];
      }
    },
    [type, sortType, tasks, currentUser, budget]
  );

  const handleFilter = () => {
    setIsFilter(!isFilter);
  };

  return (
    <Browse>
      <MapBox>
        <Map
          tasks={tasks ? tasksToDisplay(tasks) : []}
          address={`${suburb}, au`}
          distance={distance}
          isSearch={isSearch}
        />
        <div className="searchSet">
          <div className="searchBar">
            <SearchBar placeholder="Search tasks by keywords" width="600" submitSearch={submitSearch} />
          </div>
          <div className={`filterBall ${isFilter ? 'onFilter' : null}`} onClick={handleFilter}>
            <span className="filterLine1" />
            <span className="filterLine2" />
            <span className="filterLine3" />
          </div>
        </div>

        {isFilter ? (
          <ToolBar>
            <div className="tools">
              <div className="filterHeader">
                <h2>Filter</h2>
                {isTaskLoading && (
                  <Box sx={{ display: 'flex' }}>
                    <CircularProgress />
                  </Box>
                )}
                <div className="toolButtonsSet">
                  <CustomButton color="info" size="large" variant="outlined" onClick={handleReset}>
                    Reset
                  </CustomButton>
                </div>
              </div>
              <div>
                <FormControl fullWidth>
                  <h3>Category</h3>
                  <Select id="type-select" value={type} onChange={handleTypeChange}>
                    <MenuItem value={'a'}>All</MenuItem>
                    <MenuItem value={'cleaning'}>Cleaning only</MenuItem>
                    <MenuItem value={'removal'}>Moving only</MenuItem>
                    <MenuItem value={'handy'}>Repair only</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div>
                <FormControl fullWidth sx={{ mt: '20px' }}>
                  <h3>Date</h3>
                  <Select id="sort-select" value={sortType} onChange={handleSortTypeChange}>
                    <MenuItem value={'a'}>All</MenuItem>
                    <MenuItem value={'threeDays'}>Due in 3 days</MenuItem>
                    <MenuItem value={'aWeek'}>Due in a week</MenuItem>
                    <MenuItem value={'aMonth'}>Due in one month</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sliderBox budgetBox">
                <h3>
                  Task Budget{'   '}
                  <span>
                    ${budget[0]} - ${budget[1]}
                  </span>
                </h3>
                <div>
                  <Slider
                    key="budgetSlider"
                    getAriaLabel={() => 'Budget range'}
                    value={budget}
                    onChange={(e) => setBudget(e.target.value)}
                    valueLabelDisplay="auto"
                    step={10}
                    min={10}
                    max={1000}
                    sx={{ color: '#146EB4' }}
                  />
                </div>
              </div>
              <div className="locationCondition">
                <h3>Location</h3>
                <LocationBox>
                  <TextField
                    id="filled-basic"
                    name="suburb"
                    value={suburb}
                    label="Suburb"
                    onChange={(e) => setSuburb(e.target.value)}
                    error={isTypeError}
                  />
                </LocationBox>
                <div className="sliderBox">
                  <h3>
                    Distance <span>{distance} KM</span>
                  </h3>
                  <Slider
                    key="distanceSlider"
                    aria-label="Distance"
                    value={typeof distance === 'number' ? distance : 0}
                    onChange={(e, value) => setDistance(value)}
                    valueLabelDisplay="auto"
                    step={1}
                    min={1}
                    max={25}
                    sx={{ color: '#146EB4' }}
                  />
                </div>
              </div>
            </div>
          </ToolBar>
        ) : null}
        {!isLoading && displayTask && (
          <Display displayTask={displayTask}>
            {isLoading && <LoadingPage />}
            <div className="details">
              <TaskDetailCard
                details={displayTask}
                setTask={chooseTask}
                taskComments={comments ? comments : []}
                taskOffers={offers ? offers : []}
              />
              <div className="backToMap" onClick={backToBrowser}>
                <ArrowForwardIosIcon sx={{ fontSize: 30, color: '#fff' }} />
              </div>
            </div>
          </Display>
        )}
      </MapBox>
    </Browse>
  );
};

export default BrowsePage;
