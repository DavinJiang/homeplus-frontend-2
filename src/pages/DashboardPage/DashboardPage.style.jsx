import styled from 'styled-components';

export const DashboardContainer = styled.div`
  max-width: 1152px;
  min-height: 94vh;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  background-color: white;
`;
