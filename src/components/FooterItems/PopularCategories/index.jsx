import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Link from '@mui/material/Link';
import { WebLinkItem, Title, MobileLinkItem, TitleIcon } from './PopularCategories.style.jsx';

const PopularCategories = () => {
  const [isExtant, setIsExtant] = React.useState(true);

  const handleClick = () => {
    setIsExtant(!isExtant);
  };
  return (
    <List>
      <ListItemButton onClick={handleClick} sx={{ justifyContent: 'space-between', padding: '0' }}>
        <Title>Popular Categories</Title>
        <TitleIcon>{isExtant ? <AddIcon sx={{ color: '#fff' }} /> : <RemoveIcon sx={{ color: '#fff' }} />}</TitleIcon>
      </ListItemButton>

      <Collapse in={!isExtant} timeout="auto" unmountOnExit>
        <MobileLinkItem>
          <Link href="#">Cleaning Services</Link>
          <Link href="#">Removalists</Link>
          <Link href="#">Handyperson</Link>
          <Link href="#">Delivery Services</Link>
          <Link href="#">Gardening Services</Link>
          <Link href="#">Auto Electricians</Link>
          <Link href="#">All Services</Link>
        </MobileLinkItem>
      </Collapse>
      <WebLinkItem>
        <Link href="#">Cleaning Services</Link>
        <Link href="#">Removalists</Link>
        <Link href="#">Handyperson</Link>
        <Link href="#">Delivery Services</Link>
        <Link href="#">Gardening Services</Link>
        <Link href="#">Auto Electricians</Link>
        <Link href="#">All Services</Link>
      </WebLinkItem>
    </List>
  );
};
export default PopularCategories;
