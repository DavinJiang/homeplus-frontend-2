import React from 'react';
import { Box } from './CopyRight.style.jsx';

const CopyRight = () => (
  <Box>
    <p>HomePlus Pty. Ltd 2022-Present, All rights reserved</p>
  </Box>
);
export default CopyRight;
