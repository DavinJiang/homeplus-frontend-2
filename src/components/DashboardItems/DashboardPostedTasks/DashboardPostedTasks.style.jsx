import styled from 'styled-components';
// import { slide } from '../../../style';

export const Display = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  right: 10px;
  top: 0;
  height: 94vh;
  max-width: 850px;
  z-index: 99;
  .details {
    width: 850px;
    height: 100%;
    padding-top: 0;
    position: relative;
    .closeButton {
      position: absolute;
      width: 45px;
      height: 45px;
      transition: all 0.3s ease-in-out;
      top: 4.6%;
      right: 3.6%;

      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      :hover {
        cursor: pointer;
      }
    }
  }

  @media (max-width: 768px) {
    height: 94vh;
    width: 100%;
    .details {
      width: 100%;
      padding-left: 5%;
      height: 94vh;
      .closeButton {
        // display: none;
      }
    }
  }
`;
