import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@mui/material/Box';
import ListSubheader from '@mui/material/ListSubheader';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Avatar from '@mui/material/Avatar';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import stringAvatar from '../../../utils/avatar.util';
import { logOut } from '../../../store/reducers/user/user.actions';
import ListItem from './components/ListItem';
import { MenuBox } from './DashboardMenu.style';

export default function SelectedListItem({ onMenuClick }) {
  const menuRef = React.useRef(null);
  const navigate = useNavigate();
  const { user } = useSelector((state) => state.currentUser);
  const dispatch = useDispatch();
  const [selectedIndex, setSelectedIndex] = React.useState('Home');
  const [isMenu, setIsMenu] = React.useState(false);
  const avatar = { ...stringAvatar(user.name).sx };
  const children = stringAvatar(user.name).children;
  const sx = { ...avatar, width: 50, height: 50 };
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    if (index === 'Tasker Dashboard') toggleOpen();
    onMenuClick(index);
  };
  const toggleOpen = () => {
    setIsOpen(!isOpen);
  };
  const [isOpen, setIsOpen] = React.useState(false);

  const handleLogout = () => {
    dispatch(logOut());
    localStorage.removeItem('AUTH_TOKEN');

    navigate('/home', { replace: true });
  };

  React.useEffect(() => {
    function handleClickOutside(event) {
      if (menuRef.current && !menuRef.current.contains(event.target)) {
        setIsMenu(false);
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [menuRef]);

  return (
    <MenuBox isMenu={isMenu} ref={menuRef}>
      <div className="expandButton" onClick={() => setIsMenu(!isMenu)}>
        <ArrowForwardIosIcon sx={{ fontSize: 20, color: '#fff' }} />
      </div>
      <List
        sx={{
          width: '100%',
          height: '90vh',
          maxWidth: 300,
          minWidth: 210,
          paddingLeft: 5,
          paddingRight: 5,
          bgcolor: 'background.paper',
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={<ListSubheader component="div" id="nested-list-subheader"></ListSubheader>}
      >
        <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '3rem' }}>
          <ListItemAvatar>
            <Avatar sx={sx} children={children} src={user.avatar}></Avatar>
          </ListItemAvatar>
          <ListSubheader sx={{ fontSize: 18, zIndex: 0 }}>{user.name}</ListSubheader>
        </Box>

        {['Home', 'Posted Tasks'].map((title) => (
          <ListItem
            key={title}
            title={title}
            marginTop="20px"
            selected={selectedIndex}
            onClick={(event) => handleListItemClick(event, title)}
          />
        ))}
        <ListItem
          title="Tasker Dashboard"
          marginTop="20px"
          selected={selectedIndex}
          onClick={(event) => handleListItemClick(event, 'Tasker Dashboard')}
        >
          {isOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={isOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {['Tasks', 'Profile'].map((title) => (
              <ListItemButton
                key={title}
                sx={{ pl: 4, marginTop: '8px' }}
                selected={selectedIndex === title}
                onClick={(event) => handleListItemClick(event, title)}
              >
                <ListItemText primary={title} />
              </ListItemButton>
            ))}
          </List>
        </Collapse>
        <ListItem
          title="User Profile"
          marginTop="20px"
          selected={selectedIndex}
          onClick={(event) => handleListItemClick(event, 'User Profile')}
        />
        <ListItem title="Log out" marginTop="40px" onClick={handleLogout} />
      </List>
    </MenuBox>
  );
}

SelectedListItem.propTypes = {
  onMenuClick: PropTypes.func.isRequired,
};
