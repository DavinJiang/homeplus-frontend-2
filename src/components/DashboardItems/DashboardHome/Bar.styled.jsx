import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import Avatar from '@mui/material/Avatar';
import notification from '../../../assets/notification.jpg';
import vector from '../../../assets/vector.jpg';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export function CustomizedSnackbars() {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Button img="notification" variant="contained" onClick={handleClick} sx={{ display: 'felx', bgcolor: '#52AB98' }}>
        <Avatar
          alt="notification_pic"
          src={notification}
          sx={{ width: 26, height: 26 }}
          onClick={() => console.log('avatar')}
        />
        Home Clearner has received new offerts
        <Avatar alt="vector_pic" src={vector} sx={{ width: 17, height: 17 }} onClick={() => console.log('avatar')} />
      </Button>

      <Button img="notification" variant="contained" onClick={handleClick} sx={{ display: 'felx', bgcolor: '#52AB98' }}>
        <Avatar
          alt="notification_pic"
          src={notification}
          sx={{ width: 26, height: 26 }}
          onClick={() => console.log('avatar')}
        />
        Home Clearner has received new offerts
        <Avatar alt="vector_pic" src={vector} sx={{ width: 17, height: 17 }} onClick={() => console.log('avatar')} />
      </Button>

      <Button img="notification" variant="contained" onClick={handleClick} sx={{ display: 'felx', bgcolor: '#C8D8E4' }}>
        <Avatar
          alt="notification_pic"
          src={notification}
          sx={{ width: 26, height: 26 }}
          onClick={() => console.log('avatar')}
        />
        Home Clearner offer has been accepted
        <Avatar alt="vector_pic" src={vector} sx={{ width: 17, height: 17 }} onClick={() => console.log('avatar')} />
      </Button>

      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          Success!
        </Alert>
      </Snackbar>
    </Stack>
  );
}
