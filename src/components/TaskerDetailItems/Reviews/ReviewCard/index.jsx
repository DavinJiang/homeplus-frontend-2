import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import formatDistance from 'date-fns/formatDistance';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import stringAvatar from '../../../../utils/avatar.util';
import { Reviewer, RatingBox, TitileLine } from './ReviewCard.style';

const ReviewCard = ({ review }) => {
  const username = review.taskEntity.userEntity.name;
  return (
    <Card
      sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#F2F2F2',
        marginBottom: '20px',
        padding: 2,
      }}
    >
      <Reviewer>
        <Avatar {...stringAvatar(username)} />
      </Reviewer>

      <Box sx={{ display: 'flex', flexDirection: 'column', margin: '15px', width: '100%' }}>
        <TitileLine>
          <Typography sx={{ marginBottom: '10px', fontSize: 18 }}>{_.upperFirst(review.taskEntity.title)}</Typography>
          <RatingBox>
            <Rating name="read-only" value={review.rating} precision={0.5} readOnly />
            <p>{review.rating}</p>
          </RatingBox>
        </TitileLine>

        <p>{`"${review.review}"`}</p>
        <Typography sx={{ marginTop: '5px' }}>-{username}</Typography>
        <Typography sx={{ textAlign: 'right', fontSize: 12 }}>
          {formatDistance(new Date(review.created_time), new Date(), { addSuffix: true })}
        </Typography>
      </Box>
    </Card>
  );
};

ReviewCard.propTypes = {
  review: PropTypes.object.isRequired,
};

export default ReviewCard;
